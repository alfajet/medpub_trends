const svg = d3.select('svg');
// const svgContainer = d3.select('#chart-container');

const margin = 80;
const width = 1000 - 2 * margin;
const height = 500 - 2 * margin;

const chart = svg.append('g')
  .attr('transform', `translate(${margin}, ${margin})`);

const xScale = d3.scaleBand()
  .range([0, width])
  .domain(data.map((s) => s.year))
  .padding(0.3)

const yScale = d3.scaleLinear()
  .range([height, 0])
  .domain([0, max_pub * 1.1]);


const makeYLines = () => d3.axisLeft()
  .scale(yScale)

chart.append('g')
  .attr('transform', `translate(0, ${height})`)
  .call(d3.axisBottom(xScale))
  .selectAll('text')
    .style("text-anchor", "end")
    .attr("transform", "rotate(-65)");

chart.append('g')
  .call(d3.axisLeft(yScale));


chart.append('g')
  .attr('class', 'grid')
  .call(makeYLines()
    .tickSize(-width, 0, 0)
    .tickFormat('')
  );

const barGroups = chart.selectAll()
  .data(data)
  .enter()
  .append('g');

barGroups
  .append('rect')
  .attr('class', 'bar')
  .attr('x', (g) => xScale(g.year))
  .attr('y', (g) => yScale(g.value))
  .attr('height', (g) => height - yScale(g.value))
  .attr('width', xScale.bandwidth());

barGroups
  .append('text')
  .attr('class', 'value')
  .attr('x', (a) => xScale(a.year) + xScale.bandwidth() / 2)
  .attr('y', (a) => height - yScale(a.value) <= height * 0.1 ? yScale(a.value) - 10 : yScale(a.value) + 20)
  .attr('text-anchor', 'middle')
  .text((a) => `${a.value}`);

svg
  .append('text')
  .attr('class', 'label')
  .attr('x', -(height / 2) - margin)
  .attr('y', margin / 2.4)
  .attr('transform', 'rotate(-90)')
  .attr('text-anchor', 'middle')
  .text('Publications');

svg.append('text')
  .attr('class', 'label')
  .attr('x', width / 2 + margin)
  .attr('y', height + margin * 1.7)
  .attr('text-anchor', 'middle')
  .text('Years');

svg.append('text')
  .attr('class', 'title')
  .attr('x', width / 2 + margin)
  .attr('y', 40)
  .attr('text-anchor', 'middle')
  .text(chart_title);

svg.append('text')
  .attr('class', 'source')
  .attr('x', width - margin / 2)
  .attr('y', height + margin * 1.7)
  .attr('text-anchor', 'start')
  .text('Source: NCBI');
