from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "medpub_trends.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import medpub_trends.users.signals  # noqa F401
        except ImportError:
            pass
