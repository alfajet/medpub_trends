from django.apps import AppConfig


class TrendsConfig(AppConfig):
    name = 'medpub_trends.trends'
