from collections import namedtuple
import requests
import xml.etree.ElementTree as ET
from time import sleep
import re
"""
This module is a collection of helper functions to collect data from NCBI.
"""

class NCBIConnectionError(requests.exceptions.RequestException):
    pass

class DiseaseNotFoundError(ValueError):
    pass

SearchResponse = namedtuple('SearchResponse', 'count key webenv publications')
Publication = namedtuple('Publication', 'pmid title year medlinedate journal')
Journal = namedtuple('Journal', 'nlmuid issn name country')

def get_disease_publications(
    disease_name,
    years=None,
    retmax: int=100000
):
    search_string = prepare_search_query_string(disease_name, years)
    try:
        search_response = search_disease_publications(search_string, retmax)
        publications = collect_publication_data(search_response)
    except (NCBIConnectionError, DiseaseNotFoundError) as e:
        raise e

    return publications


def prepare_search_query_string(disease_name, years=None):
    search_query_string = disease_name
    if isinstance(years, tuple):
        search_query_string = search_query_string + f' AND {years[0]}:{years[1]}[pdat]'
    elif isinstance(years, int):
        search_query_string = search_query_string + f' AND {years}[pdat]'
    return search_query_string

def search_disease_publications(disease_query: str, use_history=True, retmax: int=100000, **kwargs):
    """
    Query the NCBI dataset for a given disease and returns a list of publications
    :param disease_query: disease to be searched could include name followed by a date search query
    :param retmax: max number of publications to be retrieved
    :param kwargs: reserved for future use
    :return: a list of publication ids
    """
    url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi'
    params = {
        'db': 'pubmed',
        'term': disease_query,
        'datetype': 'pdat',
        'retmax': retmax,
    }
    if use_history:
        params['usehistory'] = 'y'
    # TODO: implement a better handling of network errors
    try:
        r = requests.get(url, params=params)
        r.raise_for_status()
    # except requests.exceptions.Timeout:
    #     pass
    # except requests.exceptions.HTTPError:
    #     pass
    except requests.exceptions.RequestException as e:
        raise NCBIConnectionError('Connection error', response=e.response)

    else:
        xml_tree = ET.fromstring(r.content)
        try:
            count_elem = int(xml_tree.find('Count').text)
        except TypeError as e:
            raise e
        else:
            if count_elem == 0:
                raise DiseaseNotFoundError
            query_key = xml_tree.find('QueryKey').text if use_history else ''
            webenv = xml_tree.find('WebEnv').text if use_history else ''
            publications = [e.text for e in xml_tree.findall('IdList/Id')]
            return SearchResponse(count=count_elem, key=query_key, webenv=webenv, publications=publications)

publications_elem_xpath = {
    'pmid': 'MedlineCitation/PMID',
    'title': 'MedlineCitation/Article/ArticleTitle',
    'year': 'MedlineCitation/Article/Journal/JournalIssue/PubDate/Year',
    'medlinedate': 'MedlineCitation/Article/Journal/JournalIssue/PubDate/MedlineDate',
}

journal_elem_xpath = {
    'nlmuid': 'MedlineCitation/MedlineJournalInfo/NlmUniqueID',
    'issn': 'MedlineCitation/MedlineJournalInfo/ISSNLinking',
    'country': 'MedlineCitation/MedlineJournalInfo/Country',
    'name': 'MedlineCitation/Article/Journal/Title',
}

def collect_publication_data(search_response: SearchResponse, retmax: int=500):
    """
    Gather publication data for a given search response
    :param search_response:
    :param retmax:
    :return:
    """
    publications = []

    url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi'
    for retstart in range(0,search_response.count, retmax):
        params = {
            'db': 'pubmed',
            'WebEnv': search_response.webenv,
            'query_key': search_response.key,
            'retstart': retstart,
            'retmax': retmax,
            'retmode': 'xml',
        }
        try:
            r = requests.get(url, params=params)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            raise NCBIConnectionError('Connection error', response=e.response)
        else:
            xml_tree = ET.fromstring(r.content)
            for pub in xml_tree.findall('PubmedArticle'):
                journal_dict = {}
                for field in journal_elem_xpath.keys():
                    try:
                        journal_dict[field] = pub.find(journal_elem_xpath[field]).text
                    except AttributeError:
                        # print(f'{field} is missing.')
                        journal_dict[field] = ''
                journal_tpl = Journal(**journal_dict)

                publication_dict = {}

                for field in publications_elem_xpath.keys():
                    try:
                        publication_dict[field] = pub.find(publications_elem_xpath[field]).text
                    except AttributeError:
                        # print(f'{field} is missing.')
                        publication_dict[field] = ''
                publication_dict['journal'] = journal_tpl
                publication_tpl = Publication(**publication_dict)
                publications.append(publication_tpl)
        sleep(0.5)

    return publications


def get_year(publication:Publication):
    if publication.year:
        year = int(publication.year)
    else:
        m = re.search(r'[1-2]\d{3}', publication.medlinedate)
        if m:
            year = int(m.group(0))
        else:
            m = re.search(r'[4-9]\d', publication.medlinedate)  #Find years represented by last two digits
            if m:
                year = int(m.group(0))
            else:
                raise ValueError("Year missing")
    return year
