from django.test import TestCase
from django.urls import  resolve
from .views import DiseaseList
from django.urls import reverse
from .models import Disease

class DiseasesTest(TestCase):
    fixtures = ['fixtures.json']

    def setUp(self) -> None:
        self.disease = Disease.objects.get(slug='disease1')
        self.disease.set_statistics()

    def test_root_url_resolves_to_home(self):
        found = resolve('/')
        self.assertEqual(found.func.view_class, DiseaseList)

    def test_list_disease(self):
        response = self.client.get(reverse('trends:home'))
        self.assertContains(response, "Disease1")

    def test_get_disease1_details(self):
        response = self.client.get('/disease1')
        self.assertInHTML('<tr><td>2019</td><td>1</td></tr>', response.content.decode(response.charset))
        self.assertInHTML('<tr><td>2020</td><td>1</td></tr>', response.content.decode(response.charset))

    def test_aggregate_statistic(self):
        self.assertEqual(self.disease.publicationstatistic_set.count(), 2)
        self.assertEqual(self.disease.publicationstatistic_set.get(year=2019).nb_publications, 1)
        self.assertEqual(self.disease.publicationstatistic_set.get(year=2020).nb_publications, 1)
