from django.urls import path
from django.views.generic import TemplateView
from . import views

app_name = "trends"

urlpatterns = [
    path("", views.DiseaseList.as_view(), name="home"),
    path("new_disease", views.disease_new, name="disease_new"),
    path("<slug:slug>", views.DiseaseDetails.as_view(), name="disease"),
]
