from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.db.models import Max, Count
from .models import Disease, Journal
from .forms import DiseaseForm
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse
import json

class DiseaseList(ListView):
    model = Disease
    queryset = Disease.objects.all()


class DiseaseDetails(DetailView):
    model = Disease

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = self.get_object()
        years = [{'year': stats.year, 'value': stats.nb_publications}
                 for stats in obj.publicationstatistic_set.all().order_by('year')]
        context['year_stats'] = json.dumps(years)
        max_publication = obj.publicationstatistic_set.aggregate(Max('nb_publications'))['nb_publications__max']
        context['max_publication'] = max_publication  # Used for scaling the chart vertical axis

        pubs_by_country = Journal.objects.filter(publication__disease=obj)\
            .values('country').annotate(pubs=Count('publication')).order_by('-pubs')
        if pubs_by_country:
            context['pubs_by_country'] = list(pubs_by_country)[:10]

        return context

def disease_new(request):
    if request.method == 'POST':
        form = DiseaseForm(request.POST)
        if form.is_valid():
            disease = form.save(commit=False)
            disease.created_by = request.user
            disease.updated_by = request.user
            disease.creation_date = timezone.now()
            disease.last_update = timezone.now()
            disease.save()
            # disease.fetch_publications()
            # disease.set_statistics()
            disease.fetch_summary_data()
            return HttpResponseRedirect(reverse('trends:disease', kwargs={'slug': disease.slug}))
    else:
        form = DiseaseForm()
        return render(request,'trends/disease_form.html', {'form': form})

