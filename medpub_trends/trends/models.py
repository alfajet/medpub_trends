from django.db import models
from django.utils.text import slugify
from django.contrib.auth import get_user_model
from .utils.ncbi_requests import get_disease_publications, get_year, NCBIConnectionError, \
    DiseaseNotFoundError, prepare_search_query_string, search_disease_publications
from django.utils import timezone
from time import sleep

DEFAULT_MAX_LEN = 255


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class SlugTimeStampModel(models.Model):
    name = models.CharField(
        max_length=DEFAULT_MAX_LEN,
        # unique=True
    )
    slug = models.SlugField(
        default='',
        max_length=DEFAULT_MAX_LEN,
        editable=False,
        unique=True
    )

    # Tracking creation and last revision
    created_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET(get_sentinel_user),
        related_name='+'
    )
    creation_date = models.DateTimeField()
    updated_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET(get_sentinel_user),
        related_name='+'
    )
    last_update = models.DateTimeField()


    class Meta:
        abstract = True


class Disease(SlugTimeStampModel):
    publications = models.ManyToManyField(
        'Publication',
        blank=True,
    )

    search_year_min = models.IntegerField(blank=True, null=True)
    search_year_max = models.IntegerField(blank=True, null=True)

    def __str__(self):
        if self.search_year_min and self.search_year_max:
            obj_str = f'{self.name} ({self.search_year_min} - {self.search_year_max})'
        elif self.search_year_min:
            obj_str = f'{self.name} ({self.search_year_min} - {timezone.now().year})'
        else:
            obj_str = self.name
        return obj_str

    def save(self, *args, **kwargs):
        slug_txt = self.name
        if self.search_year_min:
            slug_txt = slug_txt + '_' + str(self.search_year_min)
        if self.search_year_max:
            slug_txt = slug_txt + '_' + str(self.search_year_max)
        self.slug = slugify(slug_txt, allow_unicode=True)
        super().save(*args, **kwargs)

    def fetch_summary_data(self):
        if self.search_year_min and self.search_year_max:
            min_year, max_year  = self.search_year_min, self.search_year_max
        elif self.search_year_min:
            min_year, max_year = self.search_year_min, timezone.now()
        elif self.search_year_max:
            min_year, max_year = (self.search_year_max - 5, self.search_year_max)
        else:
            min_year, max_year = (timezone.now().year - 5, timezone.now().year)

        for year in range(min_year, max_year + 1):
            search_query = prepare_search_query_string(self.name, year)
            search_response = search_disease_publications(search_query, use_history=False)
            stats, _ = PublicationStatistic.objects.get_or_create(
                disease=self,
                year=year
            )
            stats.nb_publications = search_response.count
            stats.save()
            sleep(0.5)

    def fetch_publications(self):
        try:
            if self.search_year_min and self.search_year_max:
                publications = get_disease_publications(self.name, (self.search_year_min, self.search_year_max))
            elif self.search_year_min:
                publications = get_disease_publications(self.name, (self.search_year_min, timezone.now().year))
            else:
                publications = get_disease_publications(self.name)
        except (NCBIConnectionError, DiseaseNotFoundError) as e:
            raise e
        else:
            for publication in publications:
                now = timezone.now()
                journal, created = Journal.objects.get_or_create(
                    issn=publication.journal.issn,
                    nlm_uid=publication.journal.nlmuid,
                    defaults={
                        'name': (publication.journal.name[:DEFAULT_MAX_LEN-5] + '...') if len(publication.journal.name) > DEFAULT_MAX_LEN else publication.journal.name,
                        'created_by': self.created_by,
                        'updated_by': self.updated_by,
                        'creation_date': now,
                        'last_update': now
                    }
                )
                if created:
                    journal.country = publication.journal.country
                    journal.save()
                try:
                    year = get_year(publication)
                except ValueError:
                    year = 0
                if publication.title:
                    title = (publication.title[:DEFAULT_MAX_LEN-5] + '...') if len(publication.title)>DEFAULT_MAX_LEN else publication.title
                else:
                    title = ''
                publication_record, created = Publication.objects.get_or_create(
                    pmid=publication.pmid,
                    defaults={
                        'journal': journal,
                        'publication_year': year,
                        'title': title
                    }
                )
                if created:
                    publication_record.save()
                self.publications.add(publication_record)


    def set_statistics(self):
        yearly_aggregates = self.publications.values('publication_year').annotate(nb_pub=models.Count('pmid'))
        for year in yearly_aggregates:
            stats, _ = PublicationStatistic.objects.get_or_create(
                disease=self,
                year=year['publication_year']
            )
            stats.nb_publications = year['nb_pub']
            stats.save()


class PublicationStatistic(models.Model):
    disease = models.ForeignKey(
        Disease,
        on_delete=models.CASCADE
    )
    year = models.IntegerField()
    nb_publications = models.IntegerField(
        null=True
    )

    class Meta:
        unique_together = ['disease', 'year']


class Journal(SlugTimeStampModel):
    nlm_uid = models.CharField(
        max_length=20,
        blank=True
    )
    issn = models.CharField(
        # primary_key=True,
        max_length=9,    # ISSN are 8 digits separated by a hyphen as follow: `xxxx-yyyy`.
        blank=True
    )
    country = models.CharField(max_length=DEFAULT_MAX_LEN, blank=True)

    class Meta:
        unique_together = ['nlm_uid', 'issn']

    def save(self, *args, **kwargs):
        self.slug = slugify(f'{self.name} {self.nlm_uid}', allow_unicode=True)
        super().save(*args, **kwargs)


class Publication(models.Model):
    # disease = models.ForeignKey(
    #     Disease,
    #     on_delete=models.CASCADE
    # )
    journal = models.ForeignKey(
        Journal,
        on_delete=models.CASCADE
    )

    pmid = models.IntegerField(
        primary_key=True
    )
    # publication_date = models.DateField()
    publication_year = models.IntegerField()
    title = models.CharField(max_length=DEFAULT_MAX_LEN, blank=True)

