from django.forms import ModelForm
from .models import Disease

class DiseaseForm(ModelForm):
    class Meta:
        model = Disease
        fields = ['name', 'search_year_min', 'search_year_max']
