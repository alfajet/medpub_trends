from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewVisitorTest(unittest.TestCase):
    def setUp(self) -> None:
        self.browser = webdriver.Firefox()

    def tearDown(self) -> None:
        self.browser.quit()

    def test_can_retrieve_disease_list(self):
        # The user opens the application and checks its homepage
        self.browser.get('http://localhost:8000')

        self.assertIn('Diseases', self.browser.title)

        header_text = self.browser.find_element_by_tag_name("h1").text
        self.assertIn('Diseases', header_text)


if __name__ == '__main__':
    unittest.main()
